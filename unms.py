# Device config URL :
# wss://127.0.0.1:8765+StYtMaepwQCcfFNwGXd475rxll4aW3xMBfwOqjyoKjl7wvRZ+allowSelfSignedCertificate

import asyncio
import pathlib
import ssl
import websockets
import base64
import uuid
from Cryptodome.Cipher import AES
import json
from Crypto import Random

MINIMUM_KEY_LENGTH = 36
AES_KEY_LENGTH = 32
IV_LENGTH = 22
AUTH_TAG_LENGTH = 22
MAC_LENGTH = 8
MIN_MESSAGE_LENGTH = IV_LENGTH + AUTH_TAG_LENGTH + MAC_LENGTH

deviceKey=bytes()
dKey=bytes()

# Key is padded so it is at least MINIMUM_KEY_LENGTH...
origKey="StYtMaepwQCcfFNwGXd475rxll4aW3xMBfwOqjyoKjl7wvRZ"
origKey=bytes(origKey, 'utf-8')
# ... hence the cut at AES_KEY_LENGTH.
origKey=base64.b64decode(origKey)

def padb64(m):
    # Pad Base64 string
    missing_padding = len(m) % 4
    if missing_padding:
        m += b'='* (4 - missing_padding)
    return(m)

def hexPrintable(buf):
    return(','.join(format(x, '02x') for x in buf))

def parseConnectMessage(m):
    return(json.loads(m))

def replyConnectMessage(i, m):
    global deviceKey
    key = bytes()
    if (len(deviceKey) == 0):
        deviceKey = Random.get_random_bytes(AES_KEY_LENGTH+4)
    key = base64.b64encode(deviceKey, b"AB")
    return(json.dumps({'id': i['id'], "type":"event", "name":"connect", 'model': i['model'], "meta": m, 'data': {'key':key.decode()}}).encode('utf-8'))

def decryptMessage(k, iv, tag, m):
    aes = AES.new(k, AES.MODE_GCM, iv)
    if (tag):
        return(aes.decrypt_and_verify(m, tag))
    else:
        return(aes.decrypt(m))

def encryptMessage(k, m):
    iv = Random.get_random_bytes(16)
    aes = AES.new(k, AES.MODE_GCM, iv)
    encmsg, tag = aes.encrypt_and_digest(m)
    return(base64.urlsafe_b64encode(iv)[0:IV_LENGTH]+base64.urlsafe_b64encode(tag)[0:AUTH_TAG_LENGTH]+base64.urlsafe_b64encode(encmsg))

def SysInfoReq():
    return(json.dumps({'id': str(uuid.uuid4()), 'meta':{}, 'type':'rpc', 'socket': 'sys', 'name': 'getSysInfo', 'request': {'GETDATA': 'sys_info'}}).encode('utf-8'))

def mactoString(m):
    return(':'.join(format(x, '02x') for x in m))

def parseMessage(msg, expectConnect = False):
    if (len(msg) <= MIN_MESSAGE_LENGTH):
        print("msg too short")
        return(None)

    iv = padb64(bytes(msg[0:IV_LENGTH], 'utf-8'))
    iv = base64.urlsafe_b64decode(iv)
    tag = padb64(bytes(msg[IV_LENGTH:IV_LENGTH+AUTH_TAG_LENGTH], 'utf-8'))
    tag = base64.urlsafe_b64decode(tag)

    encmsg = bytes()
    mac = bytes()
    
    if(expectConnect):
        mac = padb64(bytes(msg[IV_LENGTH+AUTH_TAG_LENGTH:MIN_MESSAGE_LENGTH], 'utf-8'))
        mac = base64.urlsafe_b64decode(mac)
        encmsg = padb64(bytes(msg[MIN_MESSAGE_LENGTH:], 'utf-8'))
        encmsg = base64.urlsafe_b64decode(encmsg)
    else:
        encmsg = padb64(bytes(msg[IV_LENGTH+AUTH_TAG_LENGTH:], 'utf-8'))
        encmsg = base64.urlsafe_b64decode(encmsg)

    return({"iv": iv, "tag": tag, "mac": mac, "encmsg": encmsg})

def isConnectMessage(m):
    if(m["type"] == "event" and m["name"] == "connect"):
        return(True)
    else:
        return(False)

async def sendKey(r, ws):
    ret = replyConnectMessage(r["msg"], r["meta"])
    print(ret)
    ret = encryptMessage(dKey[0:AES_KEY_LENGTH], ret).decode()
    await ws.send(ret)
    ret = encryptMessage(dKey[0:AES_KEY_LENGTH], SysInfoReq()).decode()
    await ws.send(ret)

def handleConnect(msg):
    global dKey
    parsed = parseMessage(msg, True)
    print("Connection from :", mactoString(parsed["mac"]))
    if(len(deviceKey) == 0):
        dKey = origKey
    else:
        dKey = deviceKey

    res = decryptMessage(dKey[0:AES_KEY_LENGTH], parsed["iv"], False, parsed["encmsg"])

    print(parsed)
    print(base64.b64encode(dKey, b"AB"))
    print(res)

    res = parseConnectMessage(res)
    return({"msg": res, "meta": { "key": {"type":"Buffer", "data": list(dKey[0:AES_KEY_LENGTH])}, "iv": {"type":"Buffer", "data": list(parsed["iv"])}, "authTag":{"type":"Buffer", "data": list(parsed["tag"])}, "aad":{"type":"Buffer", "data": list(parsed["mac"])}, "mac": mactoString(parsed["mac"])}})

async def Connect(request, websocket):
    r = handleConnect(request)
    if(isConnectMessage(r["msg"])):
        await sendKey(r, websocket)

async def incoming(websocket, path):
    request = await websocket.recv()
    print(request)
#    print(websocket.request_headers) # Sec-WebSocket-Protocol: unms2
    await Connect(request, websocket)

ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
localhost_pem = pathlib.Path(__file__).with_name("cert.pem")
ssl_context.load_cert_chain(localhost_pem)

start_server = websockets.serve(incoming, "*", 8765, ssl=ssl_context, ping_timeout=None)
asyncio.get_event_loop().run_until_complete(start_server)
asyncio.get_event_loop().run_forever()
